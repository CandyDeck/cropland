from pathlib import Path

import landuse_download as lud
import prodcrop_download as pcd
import prodlivestock_download as pld
import landuse_calculation as luc

'''
HAVE TO CREATE A NEW FOLDER WITH ALL THE OUTPUT FILES
'''


src_url = "http://fenixservices.fao.org/faostat/static/bulkdownloads/Inputs_LandUse_E_All_Data.zip"
src_csv = Path("Inputs_LandUse_E_All_Data_NOFLAG.csv")

src_url2 = "http://fenixservices.fao.org/faostat/static/bulkdownloads/Production_Crops_E_All_Data.zip"
src_csv2 = Path("Production_Crops_E_All_Data.csv")

src_url3 = "http://fenixservices.fao.org/faostat/static/bulkdownloads/Production_LivestockPrimary_E_All_Data.zip"
src_csv3 = Path("Production_LivestockPrimary_E_All_Data.csv")    
    
storage_root = Path("./land_use").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"


relevant_years = [lud.make_valid_fao_year(year) for year in [1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018]]

'''
Download Input land Use from Faostat
Get missing values
Refresh data
'''

land_zip = lud.download_fao_data(src_url=src_url, storage_path=download_path)
lud.extract_archive(zip_archive=land_zip, store_to=data_path)

land_all = lud.read_land_data(data_path / src_csv, relevant_years=relevant_years)
land_missing = lud.get_missing_data(land_all)


land_missing.to_csv(data_path / 'missing_land_use.csv',index = False)
land_all.to_csv(data_path / 'refreshed_land_use.csv',index = False)

'''
Download Production crops from Faostat
Get missing values
Refresh data -> get area harvested and get production in tonnes
'''

production_crop_zip = pcd.download_crop_data(src_url=src_url2, storage_path=download_path)
pcd.extract_archive(zip_archive=production_crop_zip, store_to=data_path)
    
harvest_crop_all = pcd.read_crop_data(data_path / src_csv2, relevant_years=relevant_years)
harvest_crop_missing = pcd.get_missing_data(harvest_crop_all)

    
harvest_crop_all.to_csv(data_path / 'refreshed_harvest_crop.csv',index = False)
harvest_crop_missing.to_csv(data_path / 'missing_harvest_crop.csv',index = False)

production_crop_prod_all = pcd.read_crop_prod_data(data_path / src_csv2, relevant_years=relevant_years)
production_crop_prod_all.to_csv(data_path / 'refreshed_production_crop.csv',index = False)

'''
Download Livestock production primary from Faostat
Get missing values
Refresh data
'''

Prod_livestock_zip = pld.download_crop_data(src_url=src_url3, storage_path=download_path)
pld.extract_archive(zip_archive=Prod_livestock_zip, store_to=data_path)
    
Prod_livestock_all = pld.read_ProdLivestock_data(data_path / src_csv3, relevant_years)
Prod_livestock_missing = pld.get_missing_data(Prod_livestock_all)
    
Prod_livestock_all.to_csv(data_path / 'refreshed_Prod_livestock.csv',index = False)  
Prod_livestock_missing.to_csv(data_path / 'missing_Prod_livestock.csv',index = False)

'''
From refreshed table, do all calculations in order to get values for each land use categories
'''

table=luc.make_table_per_year(relevant_years)

    