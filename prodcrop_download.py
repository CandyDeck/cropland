
import zipfile
import pandas as pd
import requests
import country_converter as coco
from pathlib import Path


def download_crop_data(src_url, storage_path, force_download=False):
    """Store the population dataset at storage path

    Parameters
    ----------

    src_url: str
        Url of the source data

    storage_path: pathlib.Path
        Location for storing the data

    force_download: boolean, optional
        If True, downloads the data even if it is already present in storage_path.
        If False (default), only downloads the data if is is not available locally.

    Returns
    -------
        Downloaded File: pathlib.Path

       """
    filename = Path(src_url.split("/")[-1])

    # Making the storage path if should not exisit
    storage_path.mkdir(parents=True, exist_ok=True)
    storage_file = storage_path / filename
    if storage_file.exists() and (force_download is False):
        return storage_file
    download = requests.get(src_url)

    # Raise exception if the file is not available
    download.raise_for_status()
    with open(storage_file, "wb") as sf:
        sf.write(download.content)

    return storage_file

def read_crop_prod_data(data_file, relevant_years=None):
    """Reads the data and returns dataframe

    Parameters
    ----------

    data_file: pathlib.Path
        Extracted fao csv file

    relevant_years: list(str)
        Relevant years defined in the main : relevant_years = list(range(1995,2021))
        Select the years of interest, in this case from 1995 to 2020

    """
    
    item_xlsx = Path("Of_interest/List_Primary production_FAO-CPA-EXIOBASE.xlsx") 
    item_sheet = 'Correspondance_FAO-CPA-EXIOBASE' 
    
    dfs = pd.read_excel(item_xlsx,item_sheet)
 
    df = pd.read_csv(data_file, encoding="latin-1")

    if relevant_years:

        country_code = list(df['Area Code'])
        converter=coco.country_converter
        
        df['ISO3']=converter.convert(names = country_code, src = 'FAOcode', to='ISO3')
        df = df[(df.ISO3 != 'not found')&(df['Element Code']==5510)]
        df = pd.merge(df,dfs[['EXIOBASE product code','EXIOBASE product']],left_on=df['Item Code'], left_index=False,right_on = dfs['FAO item code'], how = 'left')
        df = df.dropna(subset=['EXIOBASE product code'])
                
        meta_col = [col for col in df.columns if not col.startswith(("Y", "key", "Element","Area"))] 
        
        return df[meta_col + relevant_years]
    
    
    else:
        return df

    


def extract_archive(zip_archive, store_to):
    """Extract zip archive (pathlib.Path) to store_to (pathlib.Path)
    KST-CD: If it is really(!) simple you can get away with very short docstrings - rules are fluid
    """
    with zipfile.ZipFile(zip_archive, "r") as zf:
        zf.extractall(path=store_to)


def read_crop_data(data_file, relevant_years=None):
    """Reads the data and returns dataframe

    Parameters
    ----------

    data_file: pathlib.Path
        Extracted fao csv file

    relevant_years: list(str)
        Relevant years defined in the main : relevant_years = list(range(1995,2021))
        Select the years of interest, in this case from 1995 to 2020

    """
    
    item_xlsx = Path("Of_interest/List_Primary production_FAO-CPA-EXIOBASE.xlsx") 
    item_sheet = 'Correspondance_FAO-CPA-EXIOBASE' 
    
    dfs = pd.read_excel(item_xlsx,item_sheet)
 
    df = pd.read_csv(data_file, encoding="latin-1")

    if relevant_years:

        country_code = list(df['Area Code'])
        converter=coco.country_converter
        
        df['ISO3']=converter.convert(names = country_code, src = 'FAOcode', to='ISO3')
        df = df[(df.ISO3 != 'not found')&(df['Element Code']==5312)]
        df = pd.merge(df,dfs[['EXIOBASE product code','EXIOBASE product']],left_on=df['Item Code'], left_index=False,right_on = dfs['FAO item code'], how = 'left')
        df = df.dropna(subset=['EXIOBASE product code'])
                
        meta_col = [col for col in df.columns if not col.startswith(("Y", "key", "Element","Area"))] 
        
        return df[meta_col + relevant_years]
    
    
    else:
        return df

def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)


def get_missing_data(df):
    """Make a summary table with all missing data

    Any row with a nan is included in the resulting table

    fao_df: pandas DataFrame
        Based on raw csv read

    """
    return df[df.isnull().any(1)]


